package com.blackbelt.affinitas.di

import android.content.res.Resources
import com.blackbelt.affinitas.pictures.IPictureManager
import com.blackbelt.affinitas.pictures.PictureManager
import com.blackbelt.affinitas.view.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SystemModule {

    @Provides
    @Singleton
    fun provideResources(application: App): Resources = application.resources

    @Provides
    @Singleton
    fun providePictureManager(application: App) : IPictureManager = PictureManager(application.applicationContext)
}