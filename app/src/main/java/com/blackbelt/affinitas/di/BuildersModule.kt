package com.blackbelt.affinitas.di

import com.blackbelt.affinitas.view.camera.CameraActivity
import com.blackbelt.affinitas.view.crop.CropActivity
import com.blackbelt.affinitas.view.crop.di.CropActivityModule
import com.blackbelt.affinitas.view.gallery.FullscreenGalleryActivity
import com.blackbelt.affinitas.view.gallery.GalleryActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector
    abstract fun bindGalleryActivty(): GalleryActivity

    @ContributesAndroidInjector
    abstract fun bindCamerActivity() : CameraActivity

    @ContributesAndroidInjector(modules = arrayOf(CropActivityModule::class))
    abstract fun bindCropActivity() : CropActivity

    @ContributesAndroidInjector
    abstract fun bindFullscreenGalleryActivity() : FullscreenGalleryActivity
}