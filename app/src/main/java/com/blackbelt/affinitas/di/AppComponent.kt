package com.blackbelt.affinitas.di

import com.blackbelt.affinitas.view.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        AndroidSupportInjectionModule::class,
        SystemModule::class,
        BuildersModule::class))
interface AppComponent {

    fun inject(app: App)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: App): Builder

        fun systemModule(systemModule: SystemModule) : Builder

        fun build(): AppComponent
    }
}