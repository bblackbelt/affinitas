package com.blackbelt.affinitas.util

import android.databinding.BindingAdapter
import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator


@BindingAdapter("loadImage", "spanCount")
fun ImageView.loadImageUrl(url: Uri?, spanCount: Int = 3) {
    if (url == null) {
        return
    }
    var res = context.resources
    layoutParams.width = res.displayMetrics.widthPixels / spanCount
    layoutParams.height = res.displayMetrics.widthPixels / spanCount
    Picasso.with(this.context).load(url).resize(layoutParams.width, layoutParams.height).centerCrop().into(this)
}

@BindingAdapter("loadOriginalImage")
fun ImageView.loadOriginalImageUrl(url: Uri?) {
    Picasso.with(this.context).load(url).into(this)
}

inline fun ImageView.loadUrl(url: String, callback: PicassoCallback.() -> Unit) {
    Picasso.with(this.context).load(url).intoWithCallback(this, callback)
}

inline fun RequestCreator.intoWithCallback(target: ImageView, callback: PicassoCallback.() -> Unit) {
    this.into(target, PicassoCallback().apply(callback))
}

class PicassoCallback : Callback {

    private var onSuccess: (() -> Unit)? = null
    private var onError: (() -> Unit)? = null

    override fun onSuccess() {
        onSuccess?.invoke()
    }

    override fun onError() {
        onError?.invoke()
    }

    fun onSuccess(function: () -> Unit) {
        this.onSuccess = function
    }

    fun onError(function: () -> Unit) {
        this.onError = function
    }
}