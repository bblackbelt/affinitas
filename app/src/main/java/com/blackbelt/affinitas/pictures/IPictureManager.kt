package com.blackbelt.affinitas.pictures

import android.graphics.Bitmap
import android.net.Uri
import io.reactivex.Observable

interface IPictureManager {
    fun saveOriginalPicture(bitmap: Bitmap): Observable<String>
    fun saveCroppedBitmap(bitmap: Bitmap): Observable<String>
    fun getUploadedPictures() : Observable<List<Uri>>
}