package com.blackbelt.affinitas.pictures

import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.support.media.ExifInterface
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import java.io.FileOutputStream


class PictureManager(context: Context) : IPictureManager {

    private val mRoot = context.getExternalFilesDir(null)

    private val mOriginalPictureDir = File(mRoot, "original")

    private val mCroppedPictureDir = File(mRoot, "cropped")

    private val mLocalCroppedPictures: BehaviorSubject<List<Uri>> = BehaviorSubject.create()

    private val croppedFilesUri: MutableList<Uri> = ArrayList()

    init {
        mOriginalPictureDir.mkdir()
        mCroppedPictureDir.mkdir()

        Observable.just(mCroppedPictureDir.listFiles())
                .map { it ->
                    it.map { file -> croppedFilesUri.add(Uri.fromFile(file)) }
                    croppedFilesUri
                }
                .subscribe(mLocalCroppedPictures::onNext, Throwable::printStackTrace)
    }

    override fun saveOriginalPicture(bitmap: Bitmap): Observable<String> {
        return saveBitmap(mOriginalPictureDir, bitmap)
    }

    override fun saveCroppedBitmap(bitmap: Bitmap): Observable<String> {
        return saveBitmap(mCroppedPictureDir, bitmap)
                .map { t: String ->
                    croppedFilesUri.add(Uri.fromFile(File(t)))
                    mLocalCroppedPictures.onNext(croppedFilesUri)
                    t
                }
    }

    private fun saveBitmap(root: File, bitmap: Bitmap): Observable<String> {
        return Observable.just(bitmap)
                .subscribeOn(Schedulers.computation())
                .map { theBitmap ->
                    val file = File(root, "${System.currentTimeMillis()}.jpeg")
                    theBitmap.save(file)
                    return@map file.absolutePath
                }
    }

    override fun getUploadedPictures(): Observable<List<Uri>> {
        return mLocalCroppedPictures.hide()
    }
}

fun Bitmap.save(file: File, compressFormat: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG, compressQuality: Int = 90) {
    val fileOutputStream = FileOutputStream(file)
    compress(compressFormat, compressQuality, fileOutputStream)
    fileOutputStream.flush()
    fileOutputStream.close()
}

fun Uri.getBitmap(contentResolver: ContentResolver): Bitmap {
    val inputStream = contentResolver.openInputStream(this);
    val exi = ExifInterface(inputStream)
    val angle = exi.getRotation()

    val parcelFileDescriptor = contentResolver.openFileDescriptor(this, "r")
    val fileDescriptor = parcelFileDescriptor.fileDescriptor

    val bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, BitmapFactory.Options())
    parcelFileDescriptor.close()
    val matrix = Matrix()
    matrix.postRotate(angle)

    return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
}

fun ExifInterface.getRotation(): Float {
    val exifOrientationTag = getAttributeInt(ExifInterface.TAG_ORIENTATION, -1)
    return when (exifOrientationTag) {
        1 -> 0f
        3 -> 180f
        6 -> 90f
        8 -> 270f
        else -> {
            0f
        }
    }
}