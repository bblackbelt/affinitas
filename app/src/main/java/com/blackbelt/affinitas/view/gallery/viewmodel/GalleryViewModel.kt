package com.blackbelt.affinitas.view.gallery.viewmodel

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import android.graphics.Bitmap
import android.view.View
import com.blackbelt.affinitas.BR
import com.blackbelt.affinitas.R
import com.blackbelt.affinitas.pictures.IPictureManager
import com.blackbelt.affinitas.view.bindables.AndroidBaseItemBinder
import com.blackbelt.affinitas.view.bindables.viewmodel.BaseViewModel
import com.blackbelt.affinitas.view.camera.CameraActivity
import com.blackbelt.affinitas.view.crop.CROP_ACTIVITY_FILE_PATH
import com.blackbelt.affinitas.view.crop.CropActivity
import com.blackbelt.affinitas.view.gallery.FULLSCREEN_IMAGE_GALLERY_POSITION
import com.blackbelt.affinitas.view.gallery.FullscreenGalleryActivity
import com.blackbelt.affinitas.view.gallery.PICK_IMAGE_REQUEST
import com.blackbelt.careemkotlin.bindable.RecyclerViewClickListener
import io.reactivex.disposables.Disposables
import javax.inject.Inject

class GalleryViewModel @Inject constructor(pictureManager: IPictureManager) : BaseViewModel() {

    private val mPictureManager = pictureManager

    private var mLoadUploadedPicturesDisposable = Disposables.disposed()

    private var mBitmapDisposable = Disposables.disposed()

    var mItems: ObservableList<UploadedImageViewModel> = ObservableArrayList()

    val mTemplates: Map<Class<*>, AndroidBaseItemBinder> = hashMapOf(
            UploadedImageViewModel::class.java to AndroidBaseItemBinder(BR.imageViewModel, R.layout.image_item))

    val mFullscreenTemplates: Map<Class<*>, AndroidBaseItemBinder> = hashMapOf(
            UploadedImageViewModel::class.java to AndroidBaseItemBinder(BR.imageViewModel, R.layout.fullscreen_image_item))

    var mStartPosition : Int = 0

    init {
        mLoadUploadedPicturesDisposable = mPictureManager
                .getUploadedPictures()
                .map {
                    it.map { url -> UploadedImageViewModel(url) }
                            .filter { itemViewModel -> !mItems.contains(itemViewModel) }
                            .map { item -> mItems.add(item) }
                }.subscribe({ _ -> notifyChange() }, Throwable::printStackTrace)
    }

    fun onCameraClicked(context: Context) {
        context.startActivity(Intent(context, CameraActivity::class.java))
    }

    fun onGalleryClicked(context: Context) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        val activity = context as? Activity ?: getParentActivity()
        activity?.startActivityForResult(Intent.createChooser(intent, null), PICK_IMAGE_REQUEST)
    }

    fun saveImage(bitmap: Bitmap?) {
        if (bitmap != null) {
            val activity = getParentActivity() ?: return
            mBitmapDisposable = mPictureManager.saveOriginalPicture(bitmap)
                    .subscribe({ filePath ->
                        val intent = Intent(activity, CropActivity::class.java)
                        intent.putExtra(CROP_ACTIVITY_FILE_PATH, filePath)
                        activity.startActivity(intent)
                    }, Throwable::printStackTrace)
        }
    }

    fun getItemClickListener(): RecyclerViewClickListener {
        return object : RecyclerViewClickListener {
            override fun onItemClick(view: View, any: Any) {
                val intent = Intent(getParentActivity(), FullscreenGalleryActivity::class.java)
                intent.putExtra(FULLSCREEN_IMAGE_GALLERY_POSITION, mItems.indexOf(any))
                getParentActivity()
                        ?.startActivity(intent)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mBitmapDisposable.dispose()
        mLoadUploadedPicturesDisposable.dispose()
    }
}