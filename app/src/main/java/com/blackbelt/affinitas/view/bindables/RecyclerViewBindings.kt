package com.blackbelt.affinitas.view.bindables

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import com.blackbelt.careemkotlin.bindable.ClickableRecyclerView
import com.blackbelt.careemkotlin.bindable.RecyclerViewClickListener


private val KEY_ITEMS = -1024

@SuppressWarnings("unchecked")
@BindingAdapter("items")
fun setItems(recyclerView: RecyclerView, items: List<Any>) {
    recyclerView.setTag(KEY_ITEMS, items)
    if (recyclerView.adapter is AndroidBindableRecyclerViewAdapter) {
        (recyclerView.adapter as AndroidBindableRecyclerViewAdapter).setDataSet(items)
    }
}

@SuppressWarnings("unchecked")
@BindingAdapter("itemViewBinder")
fun setItemViewBinder(recyclerView: RecyclerView, templates: Map<Class<*>, AndroidBaseItemBinder>) {
    val tmpItems: Collection<Any>? = recyclerView.getTag(KEY_ITEMS) as Collection<Any>?
    val items: ArrayList<Any>
    items = if (tmpItems != null) {
        java.util.ArrayList(tmpItems)
    } else {
        ArrayList()
    }
    if (recyclerView.adapter is AndroidBindableRecyclerViewAdapter) {
        setItems(recyclerView, items)
        return
    }
    val adapter = AndroidBindableRecyclerViewAdapter(templates);
    recyclerView.isNestedScrollingEnabled = true
    recyclerView.setHasFixedSize(true)
    recyclerView.adapter = adapter
    setItems(recyclerView, items);
}

@BindingAdapter("itemClickListener")
fun setItemClickListener(recyclerView: RecyclerView, listener: RecyclerViewClickListener) {
    if (recyclerView is ClickableRecyclerView) {
        recyclerView.mRecyclerViewClickListener = listener
    }
}

@BindingAdapter("startPosition")
fun setStartPosition(recyclerView: RecyclerView, startPos: Int) {
    recyclerView.smoothScrollToPosition(startPos)
}
