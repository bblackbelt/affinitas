package com.blackbelt.affinitas.view

import android.app.Activity
import android.app.Application
import com.blackbelt.affinitas.di.DaggerAppComponent
import com.blackbelt.affinitas.di.SystemModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    @Inject
    lateinit var mAndroidDispatchingInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent
                .builder()
                .application(this)
                .systemModule(SystemModule())
                .build()
                .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = mAndroidDispatchingInjector
}