package com.blackbelt.affinitas.view.crop

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.blackbelt.affinitas.BR
import com.blackbelt.affinitas.R
import com.blackbelt.affinitas.view.bindables.BaseBindableActivity
import com.blackbelt.affinitas.view.crop.viewmodel.CropImageViewModel
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_crop.*
import javax.inject.Inject

const val CROP_ACTIVITY_FILE_PATH = "CROP_ACTIVITY_FILE_PATH"

class CropActivity : BaseBindableActivity() {

    @Inject
    lateinit var mCropViewModel: CropImageViewModel

    val mCropImageView: CropImageView  by lazy {
        findViewById<CropImageView>(R.id.cropImageView)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(BR.cropViewModel, mCropViewModel, R.layout.activity_crop)

        findViewById<View>(R.id.crop_image_button).setOnClickListener {
            cropImageView.getCroppedImageAsync(resources.displayMetrics.widthPixels, resources.displayMetrics.heightPixels)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.rotate_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.rotate) {
            mCropImageView.rotatedDegrees = mCropImageView.rotatedDegrees + 90
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}
