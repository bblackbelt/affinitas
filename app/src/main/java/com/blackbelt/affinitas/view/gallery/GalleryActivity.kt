package com.blackbelt.affinitas.view.gallery

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import com.android.databinding.library.baseAdapters.BR
import com.blackbelt.affinitas.R
import com.blackbelt.affinitas.pictures.getBitmap
import com.blackbelt.affinitas.view.bindables.BaseBindableActivity
import com.blackbelt.affinitas.view.gallery.viewmodel.GalleryViewModel
import javax.inject.Inject

const val PICK_IMAGE_REQUEST = 100

class GalleryActivity : BaseBindableActivity() {

    @Inject
    lateinit var mGalleryViewModel: GalleryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(BR.galleryViewModel, mGalleryViewModel, R.layout.activity_gallery)
        findViewById<RecyclerView>(R.id.uploaded_photo).layoutManager = GridLayoutManager(this, 3)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            mGalleryViewModel.saveImage(data?.data?.getBitmap(contentResolver))
        }
    }
}
