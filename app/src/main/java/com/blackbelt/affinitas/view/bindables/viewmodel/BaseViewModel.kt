package com.blackbelt.affinitas.view.bindables.viewmodel

import android.app.Activity
import android.databinding.BaseObservable
import com.blackbelt.affinitas.view.bindables.IActivityLifecycle
import java.lang.ref.WeakReference

open class BaseViewModel : BaseObservable(), IActivityLifecycle {

    private var mActivity: WeakReference<Activity>? = null

    fun setParentActivity(activity: Activity?) {
        mActivity?.clear()
        if (activity != null) {
            mActivity = WeakReference(activity)
        }
    }

    fun getParentActivity(): Activity? {
        return mActivity?.get()
    }

}
