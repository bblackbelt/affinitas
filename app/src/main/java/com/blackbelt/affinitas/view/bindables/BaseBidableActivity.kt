package com.blackbelt.affinitas.view.bindables

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import com.blackbelt.affinitas.view.bindables.viewmodel.BaseViewModel
import dagger.android.AndroidInjection

abstract class BaseBindableActivity : AppCompatActivity() {

    private  var mBaseViewModel: BaseViewModel? = null

    private lateinit var mBinding: ViewDataBinding;

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        mBaseViewModel?.onCreate(savedInstanceState)
    }

    fun setContentView(bindingVariable: Int, viewModel: BaseViewModel , @LayoutRes layoutRes: Int) {
        mBaseViewModel = viewModel
        mBinding = DataBindingUtil.setContentView<ViewDataBinding>(this, layoutRes);
        mBinding.setVariable(bindingVariable, viewModel)
        viewModel.onCreate(null)
    }

    override fun onStart() {
        super.onStart()
        mBaseViewModel?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mBaseViewModel?.onStop()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        mBaseViewModel?.setParentActivity(this)
        mBaseViewModel?.onPostCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        mBaseViewModel?.setParentActivity(null)
        mBaseViewModel?.onDestroy()
    }
}
