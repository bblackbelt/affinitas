package com.blackbelt.affinitas.view.gallery

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.support.v7.widget.RecyclerView
import com.android.databinding.library.baseAdapters.BR
import com.blackbelt.affinitas.R
import com.blackbelt.affinitas.view.bindables.BaseBindableActivity
import com.blackbelt.affinitas.view.gallery.viewmodel.GalleryViewModel
import javax.inject.Inject

const val FULLSCREEN_IMAGE_GALLERY_POSITION = "FULLSCREEN_IMAGE_GALLERY_POSITION"

class FullscreenGalleryActivity : BaseBindableActivity() {

    @Inject
    lateinit var mGalleryViewModel: GalleryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mGalleryViewModel.mStartPosition = intent.getIntExtra(FULLSCREEN_IMAGE_GALLERY_POSITION, 0)
        setContentView(BR.galleryViewModel, mGalleryViewModel, R.layout.activity_fullscreen_gallery)
        val recyclerView = findViewById<RecyclerView>(R.id.fullscreen_rv)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        PagerSnapHelper().attachToRecyclerView(recyclerView)
    }

}