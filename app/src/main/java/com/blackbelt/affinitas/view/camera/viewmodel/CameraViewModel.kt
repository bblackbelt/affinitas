package com.blackbelt.affinitas.view.camera.viewmodel

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.databinding.Bindable
import android.support.v4.app.ActivityCompat
import com.blackbelt.affinitas.R
import com.blackbelt.affinitas.pictures.IPictureManager
import com.blackbelt.affinitas.util.goToSettings
import com.blackbelt.affinitas.util.hasPermission
import com.blackbelt.affinitas.view.bindables.viewmodel.BaseViewModel
import com.blackbelt.affinitas.view.crop.CROP_ACTIVITY_FILE_PATH
import com.blackbelt.affinitas.view.crop.CropActivity
import io.fotoapparat.FotoapparatSwitcher
import io.fotoapparat.photo.BitmapPhoto
import io.fotoapparat.result.adapter.rxjava2.ObservableAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class CameraViewModel @Inject constructor(pictureManager: IPictureManager, resources: Resources) : BaseViewModel() {

    private val mPictureManager = pictureManager

    private val mResources = resources

    private var mFotoApparat: FotoapparatSwitcher? = null

    private var mFotoApparatDisposable: Disposable? = Disposables.disposed()

    private var mBitmapPhoto: BitmapPhoto? = null

    private var mPermissionText = mResources.getString(R.string.camera_permission)

    private var mClickGoToSettings = false

    private var mStartCropActivityDisposable = Disposables.disposed()

    fun hasCameraPermission(context: Context): Boolean = context.hasPermission(Manifest.permission.CAMERA)

    fun shouldShowRationale(): Boolean {
        return getParentActivity()!!.hasPermission(Manifest.permission.CAMERA)
    }

    fun askCameraPermission(activityCompat: Activity) {
        ActivityCompat.requestPermissions(activityCompat, arrayOf(Manifest.permission.CAMERA), 0)
    }

    fun askCameraPermission(context: Context) {
        val activity = context as? Activity ?: getParentActivity()
        when (mClickGoToSettings) {
            false -> {
                if (activity != null) {
                    askCameraPermission(activity)
                }
            }
            true -> {
                activity?.goToSettings()
                activity?.finish()
            }
        }
    }

    fun acquirePicture() {
        if (mFotoApparat != null) {
            mFotoApparatDisposable = mFotoApparat
                    ?.currentFotoapparat
                    ?.takePicture()
                    ?.toBitmap()
                    ?.adapt(ObservableAdapter.toObservable())
                    ?.subscribeOn(Schedulers.computation())
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe({ photo: BitmapPhoto ->
                        mBitmapPhoto = photo
                        notifyChange()
                        stopFotoApparat()
                    }, Throwable::printStackTrace) ?: Disposables.disposed()
        }
    }

    override fun onStart() {
        super.onStart()
        startFotoApparat()
    }

    override fun onStop() {
        super.onStop()
        stopFotoApparat()
    }

    private fun startFotoApparat() {
        try {
            if (mFotoApparat != null) {
                mFotoApparat?.start()
            }
        } catch (e: Exception) {
        }
    }

    private fun stopFotoApparat() {
        try {
            if (mFotoApparat != null) {
                mFotoApparat?.stop()
            }
        } catch (e: Exception) {
        }
    }

    @Bindable
    fun isCameraAcquiring(): Boolean {
        return mBitmapPhoto == null
    }

    @Bindable
    fun getPermissionText(): String {
        return mPermissionText
    }

    override fun onDestroy() {
        super.onDestroy()
        mFotoApparatDisposable?.dispose()
        mStartCropActivityDisposable.dispose()
    }

    fun setFotoApparat(fotoapparat: FotoapparatSwitcher?) {
        mFotoApparat = fotoapparat
        notifyChange()
    }

    fun checkRationale() {
        if (!shouldShowRationale()) {
            mPermissionText = mResources.getString(R.string.settings)
            notifyChange()
            mClickGoToSettings = true
        }
    }

    fun retryAcquiring() {
        mBitmapPhoto = null
        startFotoApparat()
        notifyChange()
    }

    fun startCropActivity(context: Context) {
        val bitmap = mBitmapPhoto?.bitmap ?: return
        mStartCropActivityDisposable =
                mPictureManager.saveOriginalPicture(bitmap)
                        .subscribe({ filePath ->
                            val intent = Intent(context, CropActivity::class.java);
                            intent.putExtra(CROP_ACTIVITY_FILE_PATH, filePath)
                            context.startActivity(intent)
                            (context as? Activity ?: getParentActivity())?.finish()
                        }, Throwable::printStackTrace)
    }
}