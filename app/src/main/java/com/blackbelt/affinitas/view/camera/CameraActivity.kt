package com.blackbelt.affinitas.view.camera

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.blackbelt.affinitas.BR
import com.blackbelt.affinitas.R
import com.blackbelt.affinitas.view.bindables.BaseBindableActivity
import com.blackbelt.affinitas.view.camera.viewmodel.CameraViewModel
import io.fotoapparat.Fotoapparat
import io.fotoapparat.FotoapparatSwitcher
import io.fotoapparat.parameter.LensPosition
import io.fotoapparat.view.CameraView
import javax.inject.Inject


class CameraActivity : BaseBindableActivity() {

    @Inject
    lateinit var mCameraViewModel: CameraViewModel

    private var mCameraView: CameraView? = null

    private val mBackFotoapparat: Fotoapparat by lazy {
        Fotoapparat
                .with(this)
                .into(mCameraView!!)
                .build()
    }

    private val mFrontFotoapparat: Fotoapparat  by lazy {
        Fotoapparat
                .with(this)
                .lensPosition { LensPosition.FRONT }
                .into(mCameraView!!)
                .build()
    }

    private var mFotoapparatSwitcher: FotoapparatSwitcher? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(BR.cameraViewModel, mCameraViewModel, R.layout.activity_camera)

        mCameraView = findViewById(R.id.camera)
        if (mCameraViewModel.hasCameraPermission(this)) {
            createFotoApparat()
            mCameraViewModel.setFotoApparat(mFotoapparatSwitcher)
        } else {
            mCameraViewModel.askCameraPermission(this)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            createFotoApparat()
            mFotoapparatSwitcher?.start()
        }
        mCameraViewModel.checkRationale()
    }

    private fun createFotoApparat() {
        mFotoapparatSwitcher = FotoapparatSwitcher.withDefault(mBackFotoapparat)
        mCameraViewModel.setFotoApparat(mFotoapparatSwitcher)
        invalidateOptionsMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (mFotoapparatSwitcher == null) {
            return super.onCreateOptionsMenu(menu)
        }
        val inflater = menuInflater
        inflater.inflate(R.menu.camera_switch, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.switch_camera) {
            if (mFotoapparatSwitcher?.currentFotoapparat == mBackFotoapparat) {
                mFotoapparatSwitcher?.switchTo(mFrontFotoapparat)
            } else {
                mFotoapparatSwitcher?.switchTo(mBackFotoapparat)
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}