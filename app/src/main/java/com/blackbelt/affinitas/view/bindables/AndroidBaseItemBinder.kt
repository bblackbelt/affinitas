package com.blackbelt.affinitas.view.bindables

data class AndroidBaseItemBinder(val brVariable: Int, val layoutId: Int)