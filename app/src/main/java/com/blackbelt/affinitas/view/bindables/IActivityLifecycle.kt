package com.blackbelt.affinitas.view.bindables

import android.os.Bundle

interface IActivityLifecycle {

    fun onCreate(savedInstanceState: Bundle?) {}

    fun onPostCreate(savedInstanceState: Bundle?) {}

    fun onStart() {}

    fun onStop() {}

    fun onDestroy() {}
}