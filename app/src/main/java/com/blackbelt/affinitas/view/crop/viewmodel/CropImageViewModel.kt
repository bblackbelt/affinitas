package com.blackbelt.affinitas.view.crop.viewmodel

import android.app.Activity
import android.net.Uri
import com.blackbelt.affinitas.pictures.IPictureManager
import com.blackbelt.affinitas.view.bindables.viewmodel.BaseViewModel
import com.blackbelt.affinitas.view.crop.CROP_ACTIVITY_FILE_PATH
import com.theartofdev.edmodo.cropper.CropImageView
import java.io.File
import javax.inject.Inject
import javax.inject.Named

class CropImageViewModel @Inject constructor(@Named(CROP_ACTIVITY_FILE_PATH) filePathUrl: String,
                                             pictureManager: IPictureManager) : BaseViewModel() {

    val mFilePathUrl = filePathUrl

    val mPictureManager = pictureManager

    val mFilePathUri = Uri.fromFile(File(filePathUrl))

    fun getFileUri(): Uri {
        return mFilePathUri
    }

    fun getCropImageListener(): CropImageView.OnCropImageCompleteListener {
        return CropImageView.OnCropImageCompleteListener { view, result ->
            mPictureManager.saveCroppedBitmap(result.bitmap)
                    .subscribe({ _ -> (view?.context as? Activity ?: getParentActivity())?.finish() }, Throwable::printStackTrace)
        }
    }
}