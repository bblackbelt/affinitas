package com.blackbelt.affinitas.view.crop.viewmodel

import android.databinding.BindingAdapter
import android.net.Uri
import com.theartofdev.edmodo.cropper.CropImageView

@BindingAdapter("loadImage")
fun loadImageToCrop(imageView: CropImageView, uri: Uri) {
    imageView.guidelines = CropImageView.Guidelines.ON
    imageView.setImageUriAsync(uri)
}

@BindingAdapter("cropImageListener")
fun setOnCropImageCompleteListener(imageView: CropImageView, listener: CropImageView.OnCropImageCompleteListener) {
    imageView.setOnCropImageCompleteListener(listener)
}


