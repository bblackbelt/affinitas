package com.blackbelt.affinitas.view.gallery.viewmodel

import android.net.Uri

data class UploadedImageViewModel(val imageUrl : Uri)
