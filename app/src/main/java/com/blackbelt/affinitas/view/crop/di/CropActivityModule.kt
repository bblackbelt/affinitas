package com.blackbelt.affinitas.view.crop.di

import com.blackbelt.affinitas.view.crop.CROP_ACTIVITY_FILE_PATH
import com.blackbelt.affinitas.view.crop.CropActivity
import dagger.Module
import dagger.Provides
import javax.inject.Named


@Module
class CropActivityModule {

    @Provides
    @Named(CROP_ACTIVITY_FILE_PATH)
    fun provideFilePath(activity: CropActivity) : String = activity.intent.getStringExtra(CROP_ACTIVITY_FILE_PATH)
}